# moneyxchange.io - Backend

This repo contains the backend project 

## Setup

Fork or download this repo and import it as maven project in your IDE

This project uses mongodb as database  
Run the following statements:  
`use money_exchange_db`  
`db.createCollection('exchanges')`  
`db.exchanges.insertOne({base: "USD", date: "2018-03-19", rates: {EUR: 0.81294}})`