package com.belatrix.exercise.exchange;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/latest")
public class ExchangeController {
	
	@Autowired
	private ExchangeService exchangeService;

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(method = RequestMethod.GET, value = "")
	public Exchange getExchange(@RequestParam(required = false) String base, 
				@RequestParam(required = false) String symbol) {
		return exchangeService.getExchange(base, symbol);
	}
	
	@ExceptionHandler(NoSuchElementException.class)
	public Map<String, String> handleException(HttpServletRequest req, Exception e) 
	{
		Map<String, String> errorMessage = new HashMap<String, String>();
		errorMessage.put("error", "Invalid base");
		return errorMessage; 
	}
	
}
