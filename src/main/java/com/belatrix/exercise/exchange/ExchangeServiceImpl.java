package com.belatrix.exercise.exchange;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExchangeServiceImpl implements ExchangeService {
	
	@Autowired
	ExchangeRepository exchangeRepository;

	@Override
	public Exchange getExchange(String base, String symbol) {
		return exchangeRepository.findByBaseAndShowRate(base, symbol).get();
	}
	
}
