package com.belatrix.exercise.exchange;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface ExchangeRepository extends MongoRepository<Exchange, String> {

	@Query("{ 'base' : ?#{[0]}, ?#{'rates.' + [1]} : {$exists : 1}}")
	Optional<Exchange> findByBaseAndShowRate(String base, String symbol);

}
