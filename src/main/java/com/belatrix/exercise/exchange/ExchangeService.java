package com.belatrix.exercise.exchange;

public interface ExchangeService {

	public Exchange getExchange(String base, String symbol);
}
